import styled from "styled-components";

export const Card = styled.div`
        display: flex;
        border-radius: 20px;
        width: 300px;
        height: 400px;
        overflow: hidden;
        background: url(${prop => prop.image});
        background-repeat: no-repeat;
        background-position: center center;
        background-size: auto 100%;
        transition: all .3s;
        &:hover{
            box-shadow: 0px 5px 10px 2px rgba(34, 60, 80, 1);
            background-size: auto 110%;
        }
    `;

export const CardContent = styled.div`
        display: flex;
        flex-direction: column;
        width: 100%;
        justify-content: flex-end;
        padding: 20px 20px;
        gap: 10px;
        background: linear-gradient(to bottom, rgba(102, 102, 116, 0), rgba(102, 102, 116, 1));
        transition: all .3s;
        opacity: 0;
        cursor: pointer;
        &:hover{
            opacity: 1;
        }
    `;

export const Title = styled.h1`
        font-size: 1.5em;
        text-align: center;
        color: gold;
    `;

export const Container = styled.section`
        padding: 1em 2em;
        display: flex;
        aling-items: center;
        justify-content: center;
    `;

export const List = styled.div`
        display: inline-flex;
        flex-wrap: wrap;
        gap: 40px;
        justify-content: center;
    `;

export const H3 = styled.h3`
        font-size: 1.3em;
        text-align: center;
        color: gold;
        margin: 0;
    `

export const Text = styled.p`
    font-size: 1em;
    color: gold;
    margin: 0;
`

export const ButtonPrimary = styled.button`
        font-size: 1em;
        text-align: center;
        padding: 10px 20px;
        color: gold;
        background: black;
        outline: none;
        border: 1px solid black;
        transition: all .3s;
        border-radius: 0px;
        cursor: pointer;
        &:hover{
            border: 1px solid gold;
        }
    `

export const ButtonSecondary = styled.button`
    font-size: 1em;
    text-align: center;
    padding: 10px 20px;
    color: black;
    background: none;
    outline: none;
    border: 1px solid black;
    transition: all .3s;
    border-radius: 0px;
    cursor: pointer;
    &:hover{
        border: 1px solid gold;
        color: gold;
    }
`

export const Popup = styled.div`
        width: fit-content;
        height: fit-content;
        max-width: 80vw;
        display: inline-flex;
        gap: 40px;
        padding: 20px 40px 20px 20px;
        box-shadow: 0px 5px 10px 2px rgba(34, 60, 80, 1);
        background: rgb(102, 102, 116);
        border-radius: 40px;
        position: relative;
        overflow: hidden;
    `

export const Modal = styled.div`
        position: fixed;
        top: 0;
        left: 0;
        width: 100vw;
        height: 100vh;
        background: rgba(34, 60, 80, 0.4);
        display: ${prop => prop.show? 'flex': 'none'};
        align-items: center;
        justify-content: center;
        z-index: 2;
    `

export const Rows = styled.div`
        display: flex;
        flex-direction: column;
    `

export const Columns = styled.div`
        display: inline-flex;
        flex-wrap: wrap;
    `

export const Image = styled.div`
        width: ${prop => prop.width};
        height: ${prop => prop.height};
        background: url(${prop => prop.src});
        background-repeat: no-repeat;
        background-position: center center;
        background-size: auto 100%;
        border-radius: 20px;
    `

export const Input = styled.input`
        font-size: 1em;
        text-align: start;
        padding: 10px 20px;
        color: gold;
        background: black;
        outline: none;
        border: 1px solid black;
        transition: all .3s;
        border-radius: 0px;
        &:focus, &:hover{
            border: 1px solid gold;
        }
        &::placeholder{
            color: white;
            opacity: 0.5;
        }
    `