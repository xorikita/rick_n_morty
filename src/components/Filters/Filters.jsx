import React, {useState} from "react";
import GroupButton from "../GoupButton/GroupButton";
import { ButtonPrimary, Input, Columns, H3, Container} from "../Styled";

export default function Filters({params, setParams, setPage}){
    const [name, setName] = useState('')
    const [status, setStatus] = useState('')
    const [species, setSpecies] = useState('')
    const [type, setType] = useState('')
    const [gender, setGender] = useState('')
    const Filter = (params) => {
        let name_param = name? `&name=${name}`: ''
        let status_param = status? `&status=${status}`: ''
        let species_param = species? `&species=${species}`: ''
        let type_param = type? `&type=${type}`: ''
        let gender_param = gender? `&gender=${gender}`: ''
        let new_params = `${name_param}${status_param}${species_param}${type_param}${gender_param}`
        setPage(1)
        setParams(new_params)
    }
    return(
        <Container>
            <Columns style={{justifyContent: 'center', alignItems: "center"}}>
                <H3 style={{marginRight: '20px'}}>Filters</H3>
                <Input placeholder="Name" onInput={(e) => setName(e.target.value)} value={name}/>
                <GroupButton value={status} setValue={setStatus} title='Status' options={['Alive', 'Dead', 'unknown']}/>
                <Input placeholder="Species" onInput={(e) => setSpecies(e.target.value)} value={species}/>
                <Input placeholder="Type" onInput={(e) => setType(e.target.value)} value={type}/>
                <GroupButton value={gender} setValue={setGender} title='Gender' options={['Female', 'Male', 'Genderless', 'unknown']}/>
                <ButtonPrimary onClick={() => Filter(params)}>Filter</ButtonPrimary>
            </Columns>
        </Container>
    )
}