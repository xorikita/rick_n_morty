import React, {useState, useRef} from "react";
import { ButtonPrimary, Rows } from "../Styled";

export default function GroupButton({title, options, value, setValue}){
    const rowsRef = useRef(null);
    const [open, setOpen] = useState(false)
    const Choose = (e) => {
        setValue(e.target.name)
        setOpen(false)
    }
    return(
        <div style={{position: 'relative', width: `${rowsRef.current?.offsetWidth}px`}}>
            <ButtonPrimary onClick={() => setOpen(!open)} style={{width: '100%', color: value? '':'rgba(255, 255, 255, 0.5)'}}>{value? value: title}</ButtonPrimary>
            <Rows ref={rowsRef} style={{position: 'absolute', top: '100%', left: '0', height: open?'fit-content':'0%', transition: 'all .3s', zIndex: '2', overflow: 'hidden'}}>
                <ButtonPrimary name="" onClick={(e) => Choose(e)}>All</ButtonPrimary>
                {options?.map(option => {
                    return(
                        <ButtonPrimary name={option} key={option} onClick={(e) => Choose(e)}>{option}</ButtonPrimary>
                    )
                })}
            </Rows>
        </div>
    )
}