import React, {useState} from "react";
import CharacterInfo from "../CharacterInfo/CharacterInfo";
import { Card, CardContent, H3, Popup, Modal, Text, ButtonSecondary} from "../Styled";      

export default function CharacterCard({data}){
    const [popup, setPopup] = useState(false)
    const showPopup = (e) => {
        e.preventDefault();
        e.stopPropagation();
        setPopup(true);
    }
    const hidePopup = (e) => {
        e.preventDefault();
        e.stopPropagation();
        setPopup(false);
    }
    return(
        <Card image={data.image} onClick={(e) => showPopup(e)}>
            <CardContent>
                <H3>{data.name}</H3>
                <Text>Status: {data.status}</Text>
                <Text>Gender: {data.gender}</Text>
            </CardContent>
            <Modal show={popup} onClick={(e) => hidePopup(e)}>
                <Popup onClick={(e) => e.stopPropagation()}>
                    <CharacterInfo data={data}/>
                    <ButtonSecondary onClick={(e) => hidePopup(e)} style={{position: 'absolute', right: '0px', top: '0px', border: 'none'}}>✖</ButtonSecondary>
                </Popup>
            </Modal>
        </Card>
    )
}