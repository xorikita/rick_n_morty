import React from "react";
import { Columns, Rows, Image, Text, H3 } from "../Styled";

export default function CharacterInfo({data}){
    return(
        <Columns style={{gap: '40px'}}>
            <Image src={data.image} width='300px' height='300px'/>
            <Rows style={{gap: '5px'}}>
                <H3>{data.name}</H3>
                <hr style={{background: 'gold', width: '100%'}}/>
                <Text>gender: {data.gender}</Text>
                <Text>status: {data.status}</Text>
                <Text>location: {data.location.name}</Text>
                <Text>origin: {data.origin.name}</Text>
                <Text>species: {data.species}</Text>
                <Text>type: {data.type}</Text>
            </Rows>
        </Columns>
    )
}