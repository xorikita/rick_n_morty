import React, {useState, useEffect} from 'react';
import './App.css';
import API from './api/api';
import CharacterCard from './components/Card/CharacterCard';
import Filters from './components/Filters/Filters';
import { Container, Title, List, ButtonPrimary, Text } from './components/Styled';

function App() {
  const [page, setPage] = useState(1)
  const [characters, setCharacters] = useState([])
  const [info, setInfo] = useState([])
  const [params, setParams] = useState('')
  useEffect(() => {
    window.scrollTo(0, 0)
    API.get(`character/?page=${page}${params}`)
    .then(res => {
      setCharacters(res.data.results)
      setInfo(res.data.info)
    })
    .catch((error) => {
      setCharacters([])
      setInfo([])
    })
  }, [page, params]);
  return (
    <div className="App">
      <Container>
        <Title>
          Characters
        </Title>
      </Container>
      <Filters params={params} setParams={setParams} setPage={setPage}/>
      <Container>
        <List>
          {
            characters?.map(character => {
              return(
                <CharacterCard data={character} key={character.id}/>
              )
            })
          }
        </List>
      </Container>
      <Container style={{gap: '20px', alignItems: 'center'}}>
        {page > 1? <ButtonPrimary onClick={() => setPage(page - 1)}>Back</ButtonPrimary>: ''}
        <Text>{page}/{info.pages}</Text>
        {page < info.pages? <ButtonPrimary onClick={() => setPage(page + 1)}>Next</ButtonPrimary>: ''}
      </Container>
    </div>
  );
}

export default App;
